import bcrypt from 'bcrypt';
import { randomBytes } from 'crypto';
import { NextFunction, Request, Response } from 'express';
import {
   createUser,
   deleteUser,
   getAllUsers,
   getUserByEmail,
   getUserById,
   getUserByIdV2,
   getUserByMobile,
   getUserByUsername,
   updateUser,
} from '../services/user.services';
export const registerUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName = '',
         lastName = '',
         email = '',
         mobile = '',
         address = '',
         profileImg = '',
         userName = '',
         password = '',
         lastLogin = '',
         lastLoginLat = '',
         lastLoginLong = '',
         status = true,
      } = req?.body;
      const saltRounds = 10;
      const hashedPassword = password ? await bcrypt.hash(password, saltRounds) : '';
      const data = {
         firstName: firstName,
         lastName: lastName,
         email: email,
         mobile: mobile,
         address: address,
         profileImg: profileImg,
         userName: userName,
         password: hashedPassword,
         loginDetails: {
            lastLogin: lastLogin,
            lastLoginLat: lastLoginLat,
            lastLoginLong: lastLoginLong,
         },
         status: status,
      };
      const savedUser = await createUser(data);
      res.status(201).json({ msg: 'User Created', data });

   } catch (error) {
      next(error);
   }
};

export const loginUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const userName = req.body.userName;
      const inputPassword = req.body.password;
      let user;
      if (!userName) {
         return res.status(400).json({ message: 'Username is required.', status: 'success' });
      } else {
         user = await getUserByUsername(userName);
      }
      console.log(user,'sdfsdfs',inputPassword)
      if (user) {
         // const token = generateToken({ userId: user._id.toString() });
         const hashedPassword = user.password;
         const isMatch = await bcrypt.compare(inputPassword, hashedPassword);
         if (isMatch) {
            return res.status(200).json({ user: user });
         } else {
            return res.status(400).json({ message: 'Invalid password.', status: 'success' });
         }
      } else {
         return res.status(400).json({ message: 'User does not exist.', status: 'success' });
      }
   } catch (error) {
      next(error);
   }
};

export const getAll = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { type, city } = req.query;
      const users = await getAllUsers(type, city);
      res.json(users);
   } catch (error) {
      next(error);
   }
};

export const getById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users: any = await getUserById(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ ...users?._doc });
   } catch (error) {
      next(error);
   }
};

export const updateById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName,
         lastName,
         email,
         mobile,
         address,
         profileImg,
         userName,
         password,
         lastLogin,
         lastLoginLat,
         lastLoginLong,
         city,
         status,
      } = req?.body;

      const user = await getUserByIdV2(req.params.id);
      if (!user) {
         return res.status(404).json({ message: 'User not found' });
      }
      // const existingUser = await getUserByEmail(email);
      // if (existingUser && existingUser._id != req.params.id) {
      //    return res.status(400).json({ message: 'Email already exists' });
      // }
      const saltRounds = 10;
      let hashedPassword: string | null = null;
      if (password) {
         hashedPassword = await bcrypt.hash(password, saltRounds);
      }

      const data = {
         firstName: firstName || user.firstName,
         lastName: lastName || user.lastName,
         email: email || user.email,
         mobile: mobile || user.mobile,
         address: address || user.address,
         profileImg: profileImg || user.profileImg,
         city: city || user.city,
         userName: userName || user.userName,
         password: hashedPassword || user.password,
         loginDetails: {
            lastLogin: lastLogin || user.loginDetails[0].lastLogin,
            lastLoginLat: lastLoginLat || user.loginDetails[0].lastLoginLat,
            lastLoginLong: lastLoginLong || user.loginDetails[0].lastLoginLong,
         },
         status: status || user.status,
      };
      const updatedUser: any = await updateUser(req.params.id, data);
      res.json(updatedUser);
   } catch (error) {
      next(error);
   }
};

export const deleteById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users = await deleteUser(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ message: 'Users deleted' });
   } catch (error) {
      next(error);
   }
};

export const resetForgetPassword = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { email, password, mobile } = req.body;
      const saltRounds = 10;
      const hashedPassword = password ? await bcrypt.hash(password, saltRounds) : '';
      const user = await getUserByEmail(email);
      const myObjectIdString = user?._id.toString();
      if (user) {
         if (user?.mobile == mobile) {
            await updateUser(myObjectIdString, {
               ...user,
               userName: user.userName,
               password: hashedPassword,
            });
            res.status(201).json({ message: 'password reset successfully' });
         } else {
            res.json({ message: 'Invalid mobile' });
         }
      } else {
         res.status(201).json({ message: 'User not register with this email' });
      }
   } catch (error) {
      next(error);
   }
};