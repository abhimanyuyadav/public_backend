import { Router } from 'express';
import {
   deleteById,
   getAll,
   getById,
   loginUser,
   registerUser,
   updateById,
   resetForgetPassword,
} from '../../controllers/users.controller';
import requireFieldsMiddleware from '../../middlewares/requireFieldsMiddleware';
const router = Router();

// GET /Users/
router.get('/', getAll);

// GET /Users/:id
router.get('/:id', getById);

// POST / registerUsers
router.post('/', registerUser);
// router.post('/', requireFieldsMiddleware(['firstName', 'mobile', 'email']), registerUser);

// POST /loginUsers
router.post('/login', requireFieldsMiddleware(['userName', 'password']),loginUser);

// PATCH /Users/:id
router.patch('/:id', updateById);

router.post('/forget-password', resetForgetPassword);

router.delete('/:id', deleteById);

export { router as usersRoute };
