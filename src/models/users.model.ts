import { Document, model, Schema } from 'mongoose';

export interface IUser extends Document {
   firstName: string;
   lastName: string;
   email: string;
   mobile: string;
   secMobile?: string;
   address: string;
   profileImg: string;
   city: string;
   userName: string;
   password: string;
   loginDetails: [
      {
         lastLogin: string;
         lastLoginLat: string;
         lastLoginLong: string;
      }
   ];
   status: boolean;
}

const userSchema = new Schema<IUser>(
   {
      firstName: { type: String, },
      lastName: { type: String, },
      email: { type: String, },
      mobile: {type: String, },
      address: { type: String, },
      profileImg: { type: String, },
      userName: { type: String },
      password: { type: String },
      loginDetails: [
         {
            lastLogin: { type: String },
            lastLoginLat: { type: String },
            lastLoginLong: { type: String },
         },
      ],
      status: { type: Boolean, },
   },
   { timestamps: true }
);

export default model<IUser>('User', userSchema);
