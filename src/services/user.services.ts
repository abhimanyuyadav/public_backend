import User, { IUser } from '../models/users.model';

export const createUser = async (data: any): Promise<IUser> => {
   const user: IUser = new User(data);
   return await user.save();
};

interface PaginationData {
   count: number;
}

interface NopaginatedData {
   count: number;
}
interface PaginatedUsers extends PaginationData {
   data: IUser[];
}

export const getAllUsers = async (type: any,city: any,): Promise<PaginatedUsers | IUser[]> => {
   const query: any = {};
   
   const count = await User.countDocuments();
   if (count) {
      const models = await User.find(query).exec();
      return {
         count,
         data: models,
      };
   } else {
      const models = await User.find(query).exec();
      const data = {
         count: count,
         data: models
      }
      return data;
   }
};

export const getUserById = async (id: string): Promise<IUser | null> => {
   // return await User.findById(id).populate(['workingCity', 'workingApartment']).exec()
   return await User.findById(id).exec();
};

export const getUserByIdV2 = async (id: string): Promise<IUser | null> => {
   return await User.findById(id);
};

export const getUserByMobile = async (mobile: string): Promise<IUser | null> => {
   return await User.findOne({ 'mobile': mobile });
};

export const getUserByEmail = async (email: string): Promise<IUser | null> => {
   return await User.findOne({ 'email': email });
};

export const getUserByUsername = async (userName?: string): Promise<IUser | null> => {
   console.log(userName,'sdgsdgsdg')
   if (!userName) {
      return null;
   }
   return await User.findOne({ 'userName': userName }).exec();
};

export const updateUser = async (id: string, data: any): Promise<IUser | null> => {
   const user = await User.findById(id);
   if (!user) {
      return null;
   }
   user.set(data);
   return await user.save();
};

export const getAllEmployee = async (): Promise<IUser[] | null> => {
   try {
      const users = await User.find({ role: { $ne: 'ADMIN' } });
      return users;
   } catch (error) {
      console.error('Error fetching users:', error);
      return null;
   }
};

export const deleteUser = async (id: string): Promise<IUser | null> => {
   return await User.findByIdAndDelete(id);
};